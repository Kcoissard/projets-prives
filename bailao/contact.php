<!doctype html>
<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Baila'O Salsa - Contact - Soirées, mariages, événements</title>
    <meta name="description"
          content="Contactez le groupe de salsa si vous souhaitez organiser un événement sur lyon et ses alentours."/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/mobile.css" media="screen and (max-width : 568px)">
    <script type="text/javascript" src="js/mobile.js"></script>
</head>
<body>
<div id="header">
    <a href="index.html" class="logo">
        <span id="titre_bailao_logo"><p class="titre_logo">Baila'O</p><img class="image_logo"
                                                                           src="images/tampon-salsa1.jpg"
                                                                           alt="salsa-logo"></span>
    </a>
    <ul id="navigation">
        <li>
            <a href="index.html">Accueil</a>
        </li>
        <li>
            <a href="evenements.html">Événements</a>
        </li>
        <li>
            <a href="le-groupe.html">El grupo</a>
        </li>
        <li>
            <a href="musique.html">Musica</a>
        </li>
        <li>
            <a href="video.html">Vidéos</a>
        </li>
        <li class="selected">
            <a href="contact.html">Contact</a>
        </li>
    </ul>
</div>
<div id="body">
    <h1><span class="title_info_contact">Message receptionné !</span></h1>
    <br/>
    <div>
        <div class="presentationGroupe">
            <h3>

                <?php
                /*
                    ********************************************************************************************
                    CONFIGURATION
                    ********************************************************************************************
                */
                // destinataire est votre adresse mail. Pour envoyer à plusieurs à la fois, séparez-les par une virgule
                $destinataire = 'salsabailao@gmail.com';

                // copie ? (envoie une copie au visiteur)
                $copie = 'oui'; // 'oui' ou 'non'

                // Messages de confirmation du mail
                $message_envoye = "Votre message nous est bien parvenu !";
                $message_non_envoye = "L'envoi du mail a échoué, veuillez réessayer SVP.";

                // Messages d'erreur du formulaire
                $message_erreur_formulaire = "Vous devez d'abord envoyer le formulaire.";
                $message_formulaire_invalide = "Vérifiez que tous les champs soient bien remplis et que l'email soit sans erreur.";

                /*
                    ********************************************************************************************
                    FIN DE LA CONFIGURATION
                    ********************************************************************************************
                */

                // on teste si le formulaire a été soumis
                if (!isset($_POST['send'])) {
                    // formulaire non envoyé
                    echo '<p>' . $message_erreur_formulaire . '</p>' . "\n";
                } else {
                    /*
                     * cette fonction sert à nettoyer et enregistrer un texte
                     */
                    function Rec($text)
                    {
                        $text = htmlspecialchars(trim($text), ENT_QUOTES);
                        if (1 === get_magic_quotes_gpc()) {
                            $text = stripslashes($text);
                        }

                        $text = nl2br($text);
                        return $text;
                    }

                    ;

                    /*
                     * Cette fonction sert à vérifier la syntaxe d'un email
                     */
                    function IsEmail($email)
                    {
                        $value = preg_match('/^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!\.)){0,61}[a-zA-Z0-9_-]?\.)+[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!$)){0,61}[a-zA-Z0-9_]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/', $email);
                        return (($value === 0) || ($value === false)) ? false : true;
                    }

                    // formulaire envoyé, on récupère tous les champs.
                    $nom = (isset($_POST['name'])) ? Rec($_POST['name']) : '';
                    $email = (isset($_POST['email'])) ? Rec($_POST['email']) : '';
                    $message = (isset($_POST['message'])) ? Rec($_POST['message']) : '';

                    // On va vérifier les variables et l'email ...
                    $email = (IsEmail($email)) ? $email : ''; // soit l'email est vide si erroné, soit il vaut l'email entré

                    if (($nom != '') && ($email != '') && ($message != '')) {
                        // les 4 variables sont remplies, on génère puis envoie le mail
                        $headers = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'From:' . $nom . ' <' . $email . '>' . "\r\n" .
                            'Reply-To:' . $email . "\r\n" .
                            'Content-Type: text/plain; charset="utf-8"; DelSp="Yes"; format=flowed ' . "\r\n" .
                            'Content-Disposition: inline' . "\r\n" .
                            'Content-Transfer-Encoding: 7bit' . " \r\n" .
                            'X-Mailer:PHP/' . phpversion();

                        // envoyer une copie au visiteur ?
                        if ($copie == 'oui') {
                            $cible = $destinataire . ';' . $email;
                        } else {
                            $cible = $destinataire;
                        };

                        // Remplacement de certains caractères spéciaux
                        $message = str_replace("&#039;", "'", $message);
                        $message = str_replace("&#8217;", "'", $message);
                        $message = str_replace("&quot;", '"', $message);
                        $message = str_replace('<br>', '', $message);
                        $message = str_replace('<br />', '', $message);
                        $message = str_replace("&lt;", "<", $message);
                        $message = str_replace("&gt;", ">", $message);
                        $message = str_replace("&amp;", "&", $message);

                        // Envoi du mail
                        $num_emails = 0;
                        $tmp = explode(';', $cible);
                        foreach ($tmp as $email_destinataire) {
                            if (mail($email_destinataire, 'bailao.fr : contact from ' . $email_destinataire, $message, $headers))
                                $num_emails++;
                        }

                        if ((($copie == 'oui') && ($num_emails == 2)) || (($copie == 'non') && ($num_emails == 1))) {
                            echo '<p>' . $message_envoye . '</p>';
                        } else {
                            echo '<p>' . $message_non_envoye . '</p>';
                        };
                    } else {
                        // une des 3 variables (ou plus) est vide ...
                        echo '<p>' . $message_formulaire_invalide . ' <a href="http://bailao.fr/contact.html">Retour au formulaire de contact</a></p>' . "\n";
                    };
                }; // fin du if (!isset($_POST['envoi']))
                ?>

            </h3>
        </div>
    </div>
    <div class="bloc_provisoir_contact">
        <!--
          <a href="https://www.facebook.com/Bailao/photos/a.1877568955808922.1073741829.1694428730789613/1927290130836804/?type=3&theater" class="coordonnees_bouton_provisoir" target="_blank"></a>
        -->

        <div class="bloc_coordonnees">
            <p><strong>Pour toute demande urgente :</strong></p>
            <!--<p><strong>Adresse mail du groupe :</strong> <a href="mailto:salsabailao@gmail.com">salsabailao@gmail.com</a></p>-->
            <p><strong>Tél. 1 : <a href="tel:+33666663942">06 66 66 39 42</a></strong></p>
            <p><strong>Tél. 2 : <a href="tel:+33628352951">06 28 35 29 51</a></strong></p>
        </div>
    </div>
</div>


<div id="footer">
    <div id="footer">
        <div>
            <p>&copy; This website was made by <a class="lien_linkedIn" href="https://coissard.net/">Kévin Coissard</a>.
                <span class="all_rights_reserved">All rights reserved.</span></p>
            <ul>
                <li>
                    <a href="https://www.facebook.com/Bailao/?fref=ts" id="facebook">facebook</a>
                </li>
            </ul>
        </div>
    </div>
</div>
</body>
</html>