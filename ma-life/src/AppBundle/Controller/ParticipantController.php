<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Event;
use AppBundle\Entity\Participant;
use AppBundle\Repository\ParticipantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Participant controller.
 *
 */
class ParticipantController extends Controller
{
    /**
     * Lists all participant entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $participants = $em->getRepository('AppBundle:Participant')->findAll();

        return $this->render('participant/index.html.twig', array(
            'participants' => $participants,
        ));
    }

    /**
     * Creates a new participant entity.
     *
     */
    public function newAction(Request $request)
    {
        $participant = new Participant();
        $form = $this->createForm('AppBundle\Form\ParticipantType', $participant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($participant);
            $em->flush();

            return $this->redirectToRoute('participant_show', array('id' => $participant->getId()));
        }

        return $this->render('participant/new.html.twig', array(
            'participant' => $participant,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a participant entity.
     *
     */
    public function showAction(Participant $participant)
    {
        $deleteForm = $this->createDeleteForm($participant);

        return $this->render('participant/show.html.twig', array(
            'participant' => $participant,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing participant entity.
     *
     */
    public function editAction(Request $request, Participant $participant)
    {
        $deleteForm = $this->createDeleteForm($participant);
        $editForm = $this->createForm('AppBundle\Form\ParticipantType', $participant);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('participant_edit', array('id' => $participant->getId()));
        }

        return $this->render('participant/edit.html.twig', array(
            'participant' => $participant,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a participant entity.
     *
     */
    public function deleteAction(Request $request, Participant $participant)
    {
        $form = $this->createDeleteForm($participant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($participant);
            $em->flush();
        }

        return $this->redirectToRoute('participant_index');
    }

    /**
     * Creates a form to delete a participant entity.
     *
     * @param Participant $participant The participant entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Participant $participant)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('participant_delete', array('id' => $participant->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @param $eventId
     * @return array
     */
    public function getParticipantsToEventAction($eventId)
    {
        /** @var ParticipantRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository(Participant::class);
        $participants = $repo->findBy(['eventParticipant' => $eventId]);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addParticipantAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $referer = $request->headers->get('referer');

        $user = $this->getUser();
        /** @var Event $event */
        $event = $em->getRepository(Event::class)->find($id);

        if ($event->getRemainingPlace() === 0) {
            $this->addFlash('bgred', 'Plus de places pour cet évènement');
            return $this->redirect($referer);
        }

        $participant = new Participant();

        $participant->setEventParticipant($event)->setUserParticipant($user);
        $event->setRemainingPlace($event->getRemainingPlace()-1);

        $em->persist($participant);
        $em->persist($event);
        $em->flush();


        $this->addFlash('bgreen', 'Vous participez maintenant à cet évènement');
        return $this->redirect($referer);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeParticipantAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $referer = $request->headers->get('referer');

        $user = $this->getUser();
        /** @var Event $event */
        $event = $em->getRepository(Event::class)->find($id);


        $participant = $em->getRepository(Participant::class)->findOneBy(['userParticipant' => $user, 'eventParticipant' => $event ]);

        $event->setRemainingPlace($event->getRemainingPlace()+1);


        $em->remove($participant);
        $em->persist($event);
        $em->flush();


        $this->addFlash('bgreen', 'Vous ne participez plus à cet évènement');
        return $this->redirect($referer);
    }
}
