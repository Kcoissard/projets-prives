<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UserTheme;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Usertheme controller.
 *
 */
class UserThemeController extends Controller
{
    /**
     * Lists all userTheme entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $userThemes = $em->getRepository('AppBundle:UserTheme')->findAll();

        return $this->render('usertheme/index.html.twig', array(
            'userThemes' => $userThemes,
        ));
    }

    /**
     * Creates a new userTheme entity.
     *
     */
    public function newAction(Request $request)
    {
        $userTheme = new Usertheme();
        $form = $this->createForm('AppBundle\Form\UserThemeType', $userTheme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($userTheme);
            $em->flush();

            return $this->redirectToRoute('usertheme_show', array('id' => $userTheme->getId()));
        }

        return $this->render('usertheme/new.html.twig', array(
            'userTheme' => $userTheme,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a userTheme entity.
     *
     */
    public function showAction(UserTheme $userTheme)
    {
        $deleteForm = $this->createDeleteForm($userTheme);

        return $this->render('usertheme/show.html.twig', array(
            'userTheme' => $userTheme,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing userTheme entity.
     *
     */
    public function editAction(Request $request, UserTheme $userTheme)
    {
        $deleteForm = $this->createDeleteForm($userTheme);
        $editForm = $this->createForm('AppBundle\Form\UserThemeType', $userTheme);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('usertheme_edit', array('id' => $userTheme->getId()));
        }

        return $this->render('usertheme/edit.html.twig', array(
            'userTheme' => $userTheme,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a userTheme entity.
     *
     */
    public function deleteAction(Request $request, UserTheme $userTheme)
    {
        $form = $this->createDeleteForm($userTheme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($userTheme);
            $em->flush();
        }

        return $this->redirectToRoute('usertheme_index');
    }

    /**
     * Creates a form to delete a userTheme entity.
     *
     * @param UserTheme $userTheme The userTheme entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(UserTheme $userTheme)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usertheme_delete', array('id' => $userTheme->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
