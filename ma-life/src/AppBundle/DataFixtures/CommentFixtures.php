<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Event;
use AppBundle\Entity\Participant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= EventFixtures::NB_EVENT; $i++) {
            /** @var Event $event */
            $event = $this->getReference('event-' . $i);

            $nbParticipant = $event->getAvailablePlace() - $event->getRemainingPlace();
            $nbComment = rand(0, $nbParticipant);
            $referenceParticipants = [];

            for (; $nbComment > 0; $nbComment--) {
                do {
                    $referenceParticipant = 'participant-' . $event->getId() . '-' . rand(1, $nbParticipant);
                } while (in_array($referenceParticipant, $referenceParticipants));
                $referenceParticipants[] = $referenceParticipant;
                /** @var Participant $participant */
                $participant = $this->getReference($referenceParticipant);

                $comment = new Comment();
                $comment->setEventComment($event);
                $comment->setUserComment($participant->getUserParticipant());
                $comment->setComment('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin bibendum risus lectus, ac ultricies arcu lacinia id. Donec id ex sed nibh tempor metus.');
                $comment->setPublish(true);
                $comment->setCreateTime(new \DateTime());
                $manager->persist($comment);
            }

            $manager->flush();
        }
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            EventFixtures::class,
        );
    }
}