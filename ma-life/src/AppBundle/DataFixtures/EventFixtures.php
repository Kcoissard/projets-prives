<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Event;
use AppBundle\Entity\Theme;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EventFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_EVENT = 30;

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < EventFixtures::NB_EVENT; $i++) {
            $hDebut = rand(1, 100);
            $debut = new \DateTime();

            $debut->add(new \DateInterval('P' . rand(0, 5) . 'D'));
            $debut->add(new \DateInterval('PT' . $hDebut . 'H'));

            $fin = new \DateTime();
            $fin->setTimestamp($debut->getTimestamp());
            $fin->add(new \DateInterval('PT1H'));

            do {
                $referenceUser = 'user-' . rand(1, UserFixtures::NB_USER);
            } while (!$this->hasReference($referenceUser));

            /** @var User $user */
            $user = $this->getReference($referenceUser);
            /** @var Theme $theme */
            $theme = $this->getReference('theme-' . rand(1, ThemeFixtures::NB_THEME));

            $event = new Event();
            $event->setUserEvent($user);
            $event->setThemeEvent($theme);
            $event->setTitle($theme->getLabel() . ' By ' . $user->getFirstName() . ' ' . $user->getLastName());
            $event->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin bibendum risus lectus, ac ultricies arcu lacinia id. Donec id ex sed nibh tempor metus.');
            $event->setStartDate($debut);
            $event->setEndDate($fin);
            $event->setAvailablePlace(rand(10, 30));
            $event->setRemainingPlace($event->getAvailablePlace() - rand(0, 10));
            $event->setPublish(true);
            $event->setCreateTime(new \DateTime());
            if ($i % 2) {
                $event->setSiteEvent($user->getSiteUser());
            } else {
                $event->setPlace('DTC');
            }
            $manager->persist($event);
            $this->addReference('event-' . ($i + 1), $event);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            SiteFixtures::class,
            ThemeFixtures::class,
        );
    }
}