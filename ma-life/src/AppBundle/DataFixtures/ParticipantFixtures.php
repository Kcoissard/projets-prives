<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Event;
use AppBundle\Entity\Participant;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ParticipantFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= EventFixtures::NB_EVENT; $i++) {
            /** @var Event $event */
            $event = $this->getReference('event-' . $i);

            $k = 0;

            $participant = new Participant();
            $participant->setEventParticipant($event);
            $participant->setUserParticipant($event->getUserEvent());
            $manager->persist($participant);
            $this->addReference('participant-' . $event->getId() . '-' . ++$k, $participant);
            $participants = [$event->getUserEvent()->getId()];

            $nbParticipant = $event->getAvailablePlace() - $event->getRemainingPlace() - 1;
            for (; $nbParticipant > 0; $nbParticipant--) {
                do {
                    do {
                        $referenceUser = 'user-' . rand(1, UserFixtures::NB_USER);
                    } while (!$this->hasReference($referenceUser));
                    /** @var User $user */
                    $user = $this->getReference($referenceUser);
                } while (in_array($user->getId(), $participants));

                $participants[] = $user->getId();


                $participant = new Participant();
                $participant->setEventParticipant($event);
                $participant->setUserParticipant($user);
                $manager->persist($participant);
                $this->addReference('participant-' . $event->getId() . '-' . ++$k, $participant);
            }
            $manager->flush();
        }
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            EventFixtures::class,
        );
    }
}