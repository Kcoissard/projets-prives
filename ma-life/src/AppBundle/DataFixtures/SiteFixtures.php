<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Site;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SiteFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $site1 = new Site();
        $site1->setLabel('La Soie');
        $manager->persist($site1);

        $site2 = new Site();
        $site2->setLabel('Corbas');
        $manager->persist($site2);

        $site3 = new Site();
        $site3->setLabel('Vesoul');
        $manager->persist($site3);

        $manager->flush();

        $this->addReference('site-1', $site1);
        $this->addReference('site-2', $site2);
        $this->addReference('site-3', $site3);
    }
}