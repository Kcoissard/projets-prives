<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Theme;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ThemeFixtures extends Fixture
{
    const NB_THEME = 5;

    public function load(ObjectManager $manager)
    {
        $labelThemes = [
            'Foot',
            'Badminton',
            'Basket',
            'Randonnée',
            'Course',
            'Piscine',
            'Cinéma',
            'Musée',
            'Restaurant',
            'Bar',
            'Bowling',
            'Karting',
            'Accrobranche',
            'Jeu de société',
            'Jeu vidéo',
        ];
        foreach ($labelThemes as $i => $labelTheme) {
            $theme = new Theme();
            $theme->setLabel($labelTheme);
            $theme->setPublish($i % 2);
            $manager->persist($theme);
            $this->addReference('theme-' . ($i + 1), $theme);
        }

        $manager->flush();
    }
}