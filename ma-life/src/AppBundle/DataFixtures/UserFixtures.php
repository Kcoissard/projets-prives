<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_USER = 50;

    public function load(ObjectManager $manager)
    {
        $noms = ['Doe', 'Martin', 'Dupont', 'Gervais', 'Raymond', 'Duffet', 'Piedalue'];
        $prenoms = ['Pierre', 'Paul', 'Martin', 'John', 'Roger', 'Antoine', 'Guillaume', 'Maurice', 'Marie', 'Ophelie'];

        $emails = [];

        for ($i = 0; $i < UserFixtures::NB_USER; $i++) {

            do {
                $prenom = $prenoms[rand(0, count($prenoms) - 1)];
                $nom = $noms[rand(0, count($noms) - 1)];
                $email = strtolower($prenom . '.' . $nom . '@exemple.fr');
            } while (in_array($email, $emails));

            $emails[] = $email;

            $user = new User();
            $user->setFirstName($prenom);
            $user->setLastName($nom);
            $user->setUsername($email);
            $user->setUsernameCanonical($email);
            $user->setEmail($email);
            $user->setEmailCanonical($email);
            $user->setEnabled(true);
            $user->setPassword('$2y$13$qLVID5OI8o0IHm7Fc70E2.wnYoEoOUWy8K6GvcFOrEjwo8PSz.Jve');
            $user->setRoles(['ROLE_ADMIN']);
            $user->setCreateTime(new \DateTime());
            $user->setSiteUser($this->getReference('site-' . rand(1, 3)));
            $manager->persist($user);
            $this->addReference('user-' . ($i + 1), $user);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            SiteFixtures::class,
        );
    }
}