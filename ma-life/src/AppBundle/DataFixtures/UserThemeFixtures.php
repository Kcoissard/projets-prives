<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\UserTheme;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserThemeFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_USER_THEME = 50;

    public function load(ObjectManager $manager)
    {
        $userThemes = [];

        for ($i = 1; $i < UserThemeFixtures::NB_USER_THEME; $i++) {
            $referenceUser = 'user-' . rand(1, UserFixtures::NB_USER);
            $referenceTheme = 'theme-' . rand(1, ThemeFixtures::NB_THEME);

            if (isset($userThemes[$referenceUser][$referenceTheme]) || !$this->hasReference($referenceUser) || !$this->hasReference($referenceTheme)) {
                continue;
            }

            if (!isset($userThemes[$referenceUser])) {
                $userThemes[$referenceUser] = [];
            }
            $userThemes[$referenceUser][$referenceTheme] = $referenceTheme;

            $userTheme = new UserTheme();
            $userTheme->setUserUserTheme($this->getReference($referenceUser));
            $userTheme->setThemeUserTheme($this->getReference($referenceTheme));
            $manager->persist($userTheme);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            ThemeFixtures::class,
        );
    }
}