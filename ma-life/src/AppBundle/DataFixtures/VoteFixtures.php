<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Event;
use AppBundle\Entity\Participant;
use AppBundle\Entity\Vote;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class VoteFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= EventFixtures::NB_EVENT; $i++) {
            /** @var Event $event */
            $event = $this->getReference('event-' . $i);

            $nbParticipant = $event->getAvailablePlace() - $event->getRemainingPlace();
            $nbVote = rand(0, $nbParticipant);
            $referenceParticipants = [];

            for (; $nbVote > 0; $nbVote--) {
                do {
                    $referenceParticipant = 'participant-' . $event->getId() . '-' . rand(1, $nbParticipant);
                } while (in_array($referenceParticipant, $referenceParticipants));
                $referenceParticipants[] = $referenceParticipant;
                /** @var Participant $participant */
                $participant = $this->getReference($referenceParticipant);

                $vote = new Vote();
                $vote->setEventVote($event);
                $vote->setUserVote($participant->getUserParticipant());
                $manager->persist($vote);
            }

            $manager->flush();
        }
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            EventFixtures::class,
        );
    }
}