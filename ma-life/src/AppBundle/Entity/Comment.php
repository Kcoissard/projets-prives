<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table(name="comment", indexes={@ORM\Index(name="fk_comment_user1_idx", columns={"user_comment_id"}), @ORM\Index(name="fk_comment_event1_idx", columns={"event_comment_id"}), @ORM\Index(name="index_comment_publish", columns={"publish"}), @ORM\Index(name="index_comment_create_time", columns={"create_time"})})
 * @ORM\Entity
 */
class Comment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=false)
     */
    private $comment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="publish", type="boolean", nullable=false)
     */
    private $publish = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_time", type="datetime", nullable=false)
     */
    private $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var \AppBundle\Entity\Event
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Event")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_comment_id", referencedColumnName="id")
     * })
     */
    private $eventComment;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_comment_id", referencedColumnName="id")
     * })
     */
    private $userComment;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set publish
     *
     * @param boolean $publish
     *
     * @return Comment
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return boolean
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * Set createTime
     *
     * @param \DateTime $createTime
     *
     * @return Comment
     */
    public function setCreateTime($createTime)
    {
        $this->createTime = $createTime;

        return $this;
    }

    /**
     * Get createTime
     *
     * @return \DateTime
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * Set eventComment
     *
     * @param \AppBundle\Entity\Event $eventComment
     *
     * @return Comment
     */
    public function setEventComment(\AppBundle\Entity\Event $eventComment = null)
    {
        $this->eventComment = $eventComment;

        return $this;
    }

    /**
     * Get eventComment
     *
     * @return \AppBundle\Entity\Event
     */
    public function getEventComment()
    {
        return $this->eventComment;
    }

    /**
     * Set userComment
     *
     * @param \AppBundle\Entity\User $userComment
     *
     * @return Comment
     */
    public function setUserComment(\AppBundle\Entity\User $userComment = null)
    {
        $this->userComment = $userComment;

        return $this;
    }

    /**
     * Get userComment
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserComment()
    {
        return $this->userComment;
    }
}
