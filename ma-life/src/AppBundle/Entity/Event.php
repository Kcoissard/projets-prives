<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="event", indexes={@ORM\Index(name="fk_event_user1_idx", columns={"user_event_id"}), @ORM\Index(name="fk_event_site1_idx", columns={"site_event_id"}), @ORM\Index(name="index_event_publish", columns={"publish"}), @ORM\Index(name="index_event_start", columns={"start_date"}), @ORM\Index(name="index_event_end", columns={"end_date"}), @ORM\Index(name="index_event_price", columns={"price"}), @ORM\Index(name="index_event_available_place", columns={"available_place"}), @ORM\Index(name="index_event_remaining_place", columns={"remaining_place"}), @ORM\Index(name="index_event_create_time", columns={"create_time"}), @ORM\Index(name="fk_event_theme1_idx", columns={"theme_event_id"})})
 * @ORM\Entity
 */
class Event
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="prerequis", type="text", length=65535, nullable=true)
     */
    private $prerequis;

    /**
     * @var integer
     *
     * @ORM\Column(name="available_place", type="integer", nullable=false)
     */
    private $availablePlace;

    /**
     * @var integer
     *
     * @ORM\Column(name="remaining_place", type="integer", nullable=false)
     */
    private $remainingPlace = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=false)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=false)
     */
    private $endDate;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=true)
     */
    private $price = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=200, nullable=true)
     */
    private $place;

    /**
     * @var integer
     *
     * @ORM\Column(name="vote", type="integer", nullable=false)
     */
    private $vote = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="publish", type="boolean", nullable=false)
     */
    private $publish = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_time", type="datetime", nullable=false)
     */
    private $createTime;

    /**
     * @var \AppBundle\Entity\Site
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="site_event_id", referencedColumnName="id")
     * })
     */
    private $siteEvent;

    /**
     * @var \AppBundle\Entity\Theme
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Theme")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="theme_event_id", referencedColumnName="id")
     * })
     */
    private $themeEvent;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_event_id", referencedColumnName="id")
     * })
     */
    private $userEvent;

    /**
     * Event constructor.
     */
    public function __construct()
    {
        $this->createTime = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Event
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set prerequis
     *
     * @param string $prerequis
     *
     * @return Event
     */
    public function setPrerequis($prerequis)
    {
        $this->prerequis = $prerequis;

        return $this;
    }

    /**
     * Get prerequis
     *
     * @return string
     */
    public function getPrerequis()
    {
        return $this->prerequis;
    }

    /**
     * Set availablePlace
     *
     * @param integer $availablePlace
     *
     * @return Event
     */
    public function setAvailablePlace($availablePlace)
    {
        $this->availablePlace = $availablePlace;

        return $this;
    }

    /**
     * Get availablePlace
     *
     * @return integer
     */
    public function getAvailablePlace()
    {
        return $this->availablePlace;
    }

    /**
     * Set remainingPlace
     *
     * @param integer $remainingPlace
     *
     * @return Event
     */
    public function setRemainingPlace($remainingPlace)
    {
        $this->remainingPlace = $remainingPlace;

        return $this;
    }

    /**
     * Get remainingPlace
     *
     * @return integer
     */
    public function getRemainingPlace()
    {
        return $this->remainingPlace;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Event
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Event
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Event
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return Event
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set vote
     *
     * @param integer $vote
     *
     * @return Event
     */
    public function setVote($vote)
    {
        $this->vote = $vote;

        return $this;
    }

    /**
     * Get vote
     *
     * @return integer
     */
    public function getVote()
    {
        return $this->vote;
    }

    /**
     * Set publish
     *
     * @param boolean $publish
     *
     * @return Event
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return boolean
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * Set createTime
     *
     * @param \DateTime $createTime
     *
     * @return Event
     */
    public function setCreateTime($createTime)
    {
        $this->createTime = $createTime;

        return $this;
    }

    /**
     * Get createTime
     *
     * @return \DateTime
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * Set siteEvent
     *
     * @param \AppBundle\Entity\Site $siteEvent
     *
     * @return Event
     */
    public function setSiteEvent(\AppBundle\Entity\Site $siteEvent = null)
    {
        $this->siteEvent = $siteEvent;

        return $this;
    }

    /**
     * Get siteEvent
     *
     * @return \AppBundle\Entity\Site
     */
    public function getSiteEvent()
    {
        return $this->siteEvent;
    }

    /**
     * Set themeEvent
     *
     * @param \AppBundle\Entity\Theme $themeEvent
     *
     * @return Event
     */
    public function setThemeEvent(\AppBundle\Entity\Theme $themeEvent = null)
    {
        $this->themeEvent = $themeEvent;

        return $this;
    }

    /**
     * Get themeEvent
     *
     * @return \AppBundle\Entity\Theme
     */
    public function getThemeEvent()
    {
        return $this->themeEvent;
    }

    /**
     * Set userEvent
     *
     * @param \AppBundle\Entity\User $userEvent
     *
     * @return Event
     */
    public function setUserEvent(\AppBundle\Entity\User $userEvent = null)
    {
        $this->userEvent = $userEvent;

        return $this;
    }

    /**
     * Get userEvent
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserEvent()
    {
        return $this->userEvent;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->title;
    }
}
