<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Participant
 *
 * @ORM\Table(name="participant", uniqueConstraints={@ORM\UniqueConstraint(name="unique_participant_user_event", columns={"user_participant_id", "event_participant_id"})}, indexes={@ORM\Index(name="fk_participant_user1_idx", columns={"user_participant_id"}), @ORM\Index(name="fk_participant_event1_idx", columns={"event_participant_id"})})
 * @ORM\Entity
 */
class Participant
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Event
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Event")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_participant_id", referencedColumnName="id")
     * })
     */
    private $eventParticipant;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_participant_id", referencedColumnName="id")
     * })
     */
    private $userParticipant;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventParticipant
     *
     * @param \AppBundle\Entity\Event $eventParticipant
     *
     * @return Participant
     */
    public function setEventParticipant(\AppBundle\Entity\Event $eventParticipant = null)
    {
        $this->eventParticipant = $eventParticipant;

        return $this;
    }

    /**
     * Get eventParticipant
     *
     * @return \AppBundle\Entity\Event
     */
    public function getEventParticipant()
    {
        return $this->eventParticipant;
    }

    /**
     * Set userParticipant
     *
     * @param \AppBundle\Entity\User $userParticipant
     *
     * @return Participant
     */
    public function setUserParticipant(\AppBundle\Entity\User $userParticipant = null)
    {
        $this->userParticipant = $userParticipant;

        return $this;
    }

    /**
     * Get userParticipant
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserParticipant()
    {
        return $this->userParticipant;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->userParticipant;
    }
}
