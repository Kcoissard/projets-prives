<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserTheme
 *
 * @ORM\Table(name="user_theme", uniqueConstraints={@ORM\UniqueConstraint(name="unique_user_theme_user_theme", columns={"user_user_theme_id", "theme_user_theme_id"})}, indexes={@ORM\Index(name="fk_user_theme_user1_idx", columns={"user_user_theme_id"}), @ORM\Index(name="fk_user_theme_theme1_idx", columns={"theme_user_theme_id"})})
 * @ORM\Entity
 */
class UserTheme
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Theme
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Theme")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="theme_user_theme_id", referencedColumnName="id")
     * })
     */
    private $themeUserTheme;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_user_theme_id", referencedColumnName="id")
     * })
     */
    private $userUserTheme;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set themeUserTheme
     *
     * @param \AppBundle\Entity\Theme $themeUserTheme
     *
     * @return UserTheme
     */
    public function setThemeUserTheme(\AppBundle\Entity\Theme $themeUserTheme = null)
    {
        $this->themeUserTheme = $themeUserTheme;

        return $this;
    }

    /**
     * Get themeUserTheme
     *
     * @return \AppBundle\Entity\Theme
     */
    public function getThemeUserTheme()
    {
        return $this->themeUserTheme;
    }

    /**
     * Set userUserTheme
     *
     * @param \AppBundle\Entity\User $userUserTheme
     *
     * @return UserTheme
     */
    public function setUserUserTheme(\AppBundle\Entity\User $userUserTheme = null)
    {
        $this->userUserTheme = $userUserTheme;

        return $this;
    }

    /**
     * Get userUserTheme
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserUserTheme()
    {
        return $this->userUserTheme;
    }
}
