<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vote
 *
 * @ORM\Table(name="vote", uniqueConstraints={@ORM\UniqueConstraint(name="unique_vote_user_event", columns={"user_vote_id", "event_vote_id"})}, indexes={@ORM\Index(name="fk_vote_user1_idx", columns={"user_vote_id"}), @ORM\Index(name="fk_vote_event1_idx", columns={"event_vote_id"})})
 * @ORM\Entity
 */
class Vote
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Event
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Event")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_vote_id", referencedColumnName="id")
     * })
     */
    private $eventVote;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_vote_id", referencedColumnName="id")
     * })
     */
    private $userVote;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventVote
     *
     * @param \AppBundle\Entity\Event $eventVote
     *
     * @return Vote
     */
    public function setEventVote(\AppBundle\Entity\Event $eventVote = null)
    {
        $this->eventVote = $eventVote;

        return $this;
    }

    /**
     * Get eventVote
     *
     * @return \AppBundle\Entity\Event
     */
    public function getEventVote()
    {
        return $this->eventVote;
    }

    /**
     * Set userVote
     *
     * @param \AppBundle\Entity\User $userVote
     *
     * @return Vote
     */
    public function setUserVote(\AppBundle\Entity\User $userVote = null)
    {
        $this->userVote = $userVote;

        return $this;
    }

    /**
     * Get userVote
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserVote()
    {
        return $this->userVote;
    }
}
