<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,
                [
                    'label' => 'Titre',
                ])
            ->add('description')
            ->add('prerequis')
            ->add('startDate', DateTimeType::class,
                [
                    'label'  => 'Date de Début',
                    'attr'   => ['class' => 'datepicker'],
                    'widget'   => 'single_text',
                ])
            ->add('endDate', DateTimeType::class,
                [
                    'label'  => 'Date de fin',
                    'attr'   => ['class' => 'datepicker'],
                    'widget'   => 'single_text',
                ])
            ->add('price', MoneyType::class,
                [
                    'label' => 'Prix'
                ])
            ->add('availablePlace')
            ->add('themeEvent')
            ->add('creatorParticipate', CheckboxType::class, [
                'mapped' => false,
                'label' => 'Je participe à mon évènement',
                'data' => true,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Event',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_event';
    }


}
