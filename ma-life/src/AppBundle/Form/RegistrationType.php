<?php
/**
 * Created by PhpStorm.
 * User: guillaume.poncet
 * Date: 20/12/2017
 * Time: 17:33
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName')
            ->add('lastName')
//            ->add('siteUser', ChoiceType::class,
//            [
//            'choices'            => [
//                'Villeurbanne La Soie' => 1,
//                'Corbas'             => 2,
//            ],
//        ])
        ;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}