<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class EventRepository
 * @package AppBundle\Repository
 */
class EventRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getFuturesEvents()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT e FROM AppBundle:Event e where e.startDate >= CURRENT_TIMESTAMP() ORDER BY e.startDate ASC'
            )
            ->getResult();
    }
}