<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ParticipantRepository
 * @package AppBundle\Repository
 */
class ParticipantRepository extends EntityRepository
{
    /**
     * @param $eventId
     * @return array
     */
    public function getParticipantsToEvent($eventId)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM AppBundle:Participant p where p.eventParticipant = ' . $eventId
            )
            ->getResult();
    }
}