<?php

namespace AppBundle\Controller;

use AppBundle\Entity\NrCharacter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Nrcharacter controller.
 *
 * @Route("character")
 */
class NrCharacterController extends Controller
{
    /**
     * Lists all nrCharacter entities.
     *
     * @Route("/", name="character_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $nrCharacters = $em->getRepository('AppBundle:NrCharacter')->findAll();

        return $this->render('nrcharacter/index.html.twig', array(
            'nrCharacters' => $nrCharacters,
        ));
    }

    /**
     * Creates a new nrCharacter entity.
     *
     * @Route("/new", name="character_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $nrCharacter = new Nrcharacter();

        $validator = $this->get('validator');
        $errors = $validator->validate($nrCharacter);

        $user = $this->getUser();
        $userId = $user->getId();

        $userCharacterNumber = $this->countUserCharacters($userId);

        //user already profile
        if (substr($request->headers->get('referer'), -6) === "/login") {
            //user already have character
            if ($userCharacterNumber > 0 ) {
                return $this->redirectToRoute('homepage');
            }
        }

        if ($userCharacterNumber >= 10) {
            $this->addFlash("danger", "Vous avez déjà 10 personnages enregistrés. Supprimez en un ou éditez le.");
            return $this->render('default/index.html.twig', [
                'user' => $user,
            ]);
        }

        $alreadyMain = $this->userAlreadyHaveMainCharacter($userId);

        $options = array(
            'userAlreadyHaveMainCharacter' => $alreadyMain,
        );

        $form = $this->createForm('AppBundle\Form\NrCharacterType', $nrCharacter, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                //hidden fields
                $nrCharacter->setDateUpdate();
                $nrCharacter->setUser($user);

                //if main character chosen, we update others
                if ($nrCharacter->isMain()) {
                    $this->putEveryCharactersNotMain($userId);
                }

                $em->persist($nrCharacter);
                $em->flush();
                $this->addFlash("success", "Personnage créé avec succès.");

                return $this->redirectToRoute('character_mylist');
            }
        }

        return $this->render('nrcharacter/new.html.twig', array(
            //'nrCharacter' => $nrCharacter,
            'form' => $form->createView(),
            'errors' => $errors,
        ));
    }

    /**
     * Finds and displays a nrCharacter entity.
     *
     * @Route("/{id}", name="character_show")
     * @Method("GET")
     */
    public function showAction(NrCharacter $nrCharacter)
    {
//        $deleteForm = $this->createDeleteForm($nrCharacter);

        $sexe = (int) $nrCharacter->isSexe();
        $imgErrorPath = $this->getParameter('web_dir') . '/img/profile-character/' . 'error.jpg';
        $imgCharacterPath = $this->getParameter('web_dir') . '/img/profile-character/' . $sexe . '-' . $nrCharacter->getRace()->getId() . '-' . $nrCharacter->getClass()->getId() . '.gif';

        //check if character's img exists on server
        if (file_exists($imgCharacterPath)) {
            $img = '/img/profile-character/' . $sexe . '-' . $nrCharacter->getRace()->getId() . '-' . $nrCharacter->getClass()->getId() . '.gif';
        } else {
            $img = '/img/profile-character/' . 'error.jpg';
        }


        return $this->render('nrcharacter/show.html.twig', array(
            'nrCharacter' => $nrCharacter,
            'img' => $img,
            //'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing nrCharacter entity.
     *
     * @Route("/{id}/edit", name="character_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, NrCharacter $nrCharacter)
    {
        $user = $this->getUser();
        $userId = $user->getId();

        if ($nrCharacter->getUser()->getId() != $userId) {
            $this->addFlash("danger", "Ce personnage ne vous appartient pas.");
            return $this->render('default/index.html.twig', [
                'user' => $user,
            ]);
        }

        $alreadyMain = false;
        if ($nrCharacter->isMain()) {
            $alreadyMain = true;
        }

        $options = array(
            'userAlreadyHaveMainCharacter' => $alreadyMain,
            'editing' => true,
        );

        //$deleteForm = $this->createDeleteForm($nrCharacter);
        $editForm = $this->createForm('AppBundle\Form\NrCharacterType', $nrCharacter, $options);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $nrCharacter->setDateupdate();

            //if main character chosen, we update others
            if ($nrCharacter->isMain()) {
                $this->putEveryCharactersNotMain($userId);
            }

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash("success", "Personnage mis à jour avec succès.");
            return $this->redirectToRoute('character_mylist');
        }

        return $this->render('nrcharacter/edit.html.twig', array(
            'nrCharacter' => $nrCharacter,
            'form' => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a nrCharacter entity.
     *
     * @Route("/{id}", name="character_delete")
     * @Method({"GET", "DELETE"})
     */
    public function deleteAction(Request $request, NrCharacter $nrCharacter)
    {
        $user = $this->getUser();
        $userId = $user->getId();

        if ($nrCharacter->getUserid()->getId() != $userId) {
            $this->addFlash("danger", "Ce personnage ne vous appartient pas.");
            return $this->render('default/index.html.twig', [
                'user' => $user,
            ]);
        }

        $characterId = $nrCharacter->getId();

        $form = $this->createDeleteForm($nrCharacter);
        $form->handleRequest($request);

        $previousUrl = $request->headers->get('referer');

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:NrCharacter')->find($characterId);

        if ($entity instanceof NrCharacter) {
            $em->remove($entity);
            $em->flush();
        } else {
            $this->addFlash("danger", "Erreur lors de la suppression, veuillez contacter l'admin.");
        }

        if (!empty($previousUrl)) {
            return $this->redirect($previousUrl);
        } else {
            return $this->redirectToRoute('character_index');
        }
    }

    /**
     * Creates a form to delete a nrCharacter entity.
     *
     * @param NrCharacter $nrCharacter The nrCharacter entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(NrCharacter $nrCharacter)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('character_delete', array('id' => $nrCharacter->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @param int $userid
     * @return mixed
     *
     */
    public function myCharactersAction($userid = null)
    {
        $user = $this->getUser();
        $userId = $user->getId();

        if ($userId == null) {
            throw new Exception(" - Fonction myCharactersAction - $.$userId=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT nrc FROM AppBundle:NrCharacter nrc 
                WHERE nrc.user=' . $userId
            . ' ORDER BY nrc.main DESC'
        );

        $usercharacters = $query->getResult();

        if (count($usercharacters) > 0) {
            return $this->render('nrcharacter/my-list.html.twig', [
                'characters' => $usercharacters,
            ]);
        } else {
            $this->addFlash("danger", "Vous n'avez pas encore créé de personnage.");
            return $this->render('nrcharacter/new.twig');
        }
    }

    
    public function getUserMainCharacterAction($userid = null)
    {

        $session = $this->get('session');
        $imgSession = $session->get('main-character');

        if (!$imgSession['img-main']) {
            $user = $this->getUser();
            $userId = $user->getId();

            if ($userId == null) {
                throw new Exception(" - Fonction getUserMainCharacterAction - $.$userId=null - ");
            }

            $em = $this->getDoctrine()->getManager();

            $query = $em->createQuery(
                'SELECT nrc FROM AppBundle:NrCharacter nrc 
                WHERE nrc.user=' . $userId
                . ' AND nrc.main=1'
            );

            $userMainCharacter = $query->getResult();

            if (count($userMainCharacter) > 0) {

                $sexe = (int) $userMainCharacter[0]->isSexe();

                $imgCharacterPath = $this->getParameter('web_dir') . '/img/profile-character/' .
                    $sexe . '-' . $userMainCharacter[0]->getRace()->getId() .
                    '-' . $userMainCharacter[0]->getClass()->getId() . '.gif';

                //check if character's img exists on server
                if (file_exists($imgCharacterPath)) {
                    $img = true;
                    $imgPath = '/img/profile-character/' . $sexe . '-' . $userMainCharacter[0]->getRace()->getId() . '-' . $userMainCharacter[0]->getClass()->getId() . '.gif';
                } else {
                    $img = false;
                    $imgPath = '/img/anonymous_icone.png';
                }

                $session->set('main-character', [
                    'img-main' => $img,
                    'img-main-path' => $imgPath,
                ]);
            }
        }

        $response = new Response();
        return $response;
    }


    #region SQL Query

    /**
     *
     * @param null $userId
     * @return bool
     */
    public function userAlreadyHaveMainCharacter($userId = null)
    {
        if ($userId == null) {
            throw new Exception(" - Fonction userAlreadyHaveMainCharacter - $.userId=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT COUNT(uc) FROM AppBundle:NrCharacter uc 
                WHERE uc.user=' . $userId
            . ' AND uc.main = 1'
        );

        $mainCharacters = $query->getResult();


        if (intval($mainCharacters[0][1]) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param null $userName
     * @return bool
     */
    public function countUserCharacters ($userId = null) {

        if ($userId == null) {
            throw new Exception(" - Fonction countUserCharacters - $.userId=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT COUNT(uc) FROM AppBundle:NrCharacter uc 
                WHERE uc.user=' . $userId
        );

        $charactersNumber = $query->getResult();

        if ($charactersNumber[0][1] > 0) {
            return $charactersNumber[0][1];
        } else {
            return false;
        }
    }

    /**
     * @param null $userId
     * @return mixed
     */
    public function putEveryCharactersNotMain($userId = null)
    {
        if ($userId == null) {
            throw new Exception(" - Fonction putEveryCharactersNotMain - $.userId=null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'UPDATE AppBundle:NrCharacter uc 
                SET uc.main = 0 
                WHERE uc.main = 1 
                AND uc.user = ' . $userId
        );

        $updateDone = $query->getResult();

        if ($updateDone > 0) {
            return true;
        } else {
            return false;
        }
    }

    #endregion

}