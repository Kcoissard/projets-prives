<?php

namespace AppBundle\Controller;

use AppBundle\Entity\NrUser;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use AppBundle\Repository\GlobalConfigRepository;

/**
 * Controller managing the user profile.
 *
 * @author Christophe Coevoet <stof@notk.org>
 */
class ProfileController extends Controller
{
    /**
     * Show the user.
     */
    public function showAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->render('@FOSUser/Profile/show.html.twig', array(
            'user' => $user,
        ));
    }

    /**
     * Edit the user.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editAction(Request $request)
    {
        /** @var NrUser $user */
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            $this->addFlash("error", "Accès refusé pour cet utilisateur.");
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');

        $form = $formFactory->createForm();

        //week dispo from form
        if(!empty($user->getApproximativeDispo())) {
            $user->setApproximativeDispo(explode('//',$user->getApproximativeDispo()));
        } else {
            $user->setApproximativeDispo(null);
        }

        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            //week dispo from form
            if(!empty($form->getNormData()->getApproximativeDispo())) {
                $user->setApproximativeDispo(implode('//',$form->getNormData()->getApproximativeDispo()));
            } else {
                $user->setApproximativeDispo(null);
            }

            //guild_id from session (here from config)
            $globalConfigRepository = new GlobalConfigRepository();
            $guildId = $globalConfigRepository->getGuildId();
            $user->setGuild($guildId);

            //guild validation
            $user->setGuildValidation(true);

            if ($form->isValid()) {
                /** @var $userManager UserManagerInterface */
                $userManager = $this->get('fos_user.user_manager');

                $userManager->updateUser($user);
                $this->addFlash("success", "Profil mis à jour avec succès.");
                return $this->redirectToRoute('homepage');

            }

            $this->addFlash("error", "Erreur lors de la mise à jour du profil.");

            if (null !== $response = $event->getResponse()) {
                return $response;
            }
        }

        return $this->render('@FOSUser/Profile/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
