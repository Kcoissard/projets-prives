<?php

namespace AppBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;

class SecurityController extends BaseController
{
    public function loginAction(\Symfony\Component\HttpFoundation\Request $request){
        $response = parent::loginAction($request);

        //do something else;
        return $response;
    }
}