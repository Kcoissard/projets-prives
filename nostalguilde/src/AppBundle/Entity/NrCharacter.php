<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * NrCharacter
 *
 * @ORM\Table(name="nr_character", uniqueConstraints={@ORM\UniqueConstraint(name="character-id_UNIQUE", columns={"id"}), @ORM\UniqueConstraint(name="character-user_id_UNIQUE", columns={"user_id"})}, indexes={@ORM\Index(name="fk_character-race_id_idx", columns={"race_id"}), @ORM\Index(name="fk_character-class_id_idx", columns={"class_id"}), @ORM\Index(name="fk_character-role_id_idx", columns={"main_role_id"}), @ORM\Index(name="fk_character-first_main_job_id_idx", columns={"first_main_job_id"}), @ORM\Index(name="fk_character-second_main_job_id_idx", columns={"second_main_job_id"}), @ORM\Index(name="fk_character-first_bonus_job_id_idx", columns={"first_bonus_job_id"}), @ORM\Index(name="fk_character-second_bonus_job_id_idx", columns={"second_bonus_job_id"}), @ORM\Index(name="fk_character-third_bonus_job_id_idx", columns={"third_bonus_job_id"})})
 * @ORM\Entity
 */
class NrCharacter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 3,
     *      max = 12,
     *      minMessage = "Le pseudo du personnage doit au minimum faire {{ limit }} caractères de long",
     *      maxMessage = "Le pseudo du personnage doit au maximum faire {{ limit }} caractères de long"
     * )
     *
     * @ORM\Column(name="nickname", type="string", length=12, nullable=false)
     */
    private $nickname;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sexe", type="boolean", nullable=false)
     */
    private $sexe = true;

    /**
     * @var integer
     *
     * @Assert\NotBlank()
     *
     * @Assert\GreaterThanOrEqual(
     *     message="Vous ne pouvez pas descendre en-dessous de 1",
     *     value=1)
     * @Assert\LessThanOrEqual(
     *     message="Vous ne pouvez pas monter au-dessus de 60",
     *     value=60)
     * @Assert\Type(type="integer")
     *
     * @ORM\Column(name="level", type="integer", nullable=false)
     */
    private $level;

    /**
     * @var boolean
     *
     * @ORM\Column(name="main", type="boolean", nullable=false)
     */
    private $main = false;

    /**
     * @var string
     *
     * @Assert\Length(
     *      max = 20000,
     *      maxMessage = "Le champs ''histoire'' doit au maximum faire {{ limit }} caractères de long"
     * )
     *
     * @ORM\Column(name="background", type="text", nullable=true)
     */
    private $background;

    /**
     * @var string
     *
     * @ORM\Column(name="official_link", type="string", length=255, nullable=true)
     */
    private $officialLink;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="dkp_total", type="integer", nullable=true)
     */
    private $dkpTotal = '0';

    /**
     * @var \NrClass
     *
     * @Assert\NotBlank()
     *
     * @ORM\ManyToOne(targetEntity="NrClass")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="class_id", referencedColumnName="id")
     * })
     */
    private $class;

    /**
     * @var \NrBonusJob
     *
     * @ORM\ManyToOne(targetEntity="NrBonusJob")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="first_bonus_job_id", referencedColumnName="id")
     * })
     */
    private $firstBonusJob;

    /**
     * @var integer
     *
     * @Assert\GreaterThanOrEqual(
     *     message="Vous ne pouvez pas descendre en-dessous de 1",
     *     value=1)
     * @Assert\LessThanOrEqual(
     *     message="Vous ne pouvez pas monter au-dessus de 300",
     *     value=300)
     *
     * @Assert\Type(type="integer")
     *
     * @ORM\Column(name="first_bonus_job_level", type="smallint", nullable=true)
     */
    private $firstBonusJobLevel;

    /**
     * @var \NrMainJob
     *
     * @ORM\ManyToOne(targetEntity="NrMainJob")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="first_main_job_id", referencedColumnName="id")
     * })
     */
    private $firstMainJob;

    /**
     * @var integer
     *
     * @Assert\GreaterThanOrEqual(
     *     message="Vous ne pouvez pas descendre en-dessous de 1",
     *     value=1)
     * @Assert\LessThanOrEqual(
     *     message="Vous ne pouvez pas monter au-dessus de 300",
     *     value=300)
     *
     * @ORM\Column(name="first_main_job_level", type="smallint", nullable=true)
     */
    private $firstMainJobLevel;

    /**
     * @var \NrRace
     *
     * @Assert\NotBlank()
     *
     * @ORM\ManyToOne(targetEntity="NrRace")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="race_id", referencedColumnName="id")
     * })
     */
    private $race;

    /**
     * @var \NrRole
     *
     * @Assert\NotBlank()
     *
     * @ORM\ManyToOne(targetEntity="NrRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="main_role_id", referencedColumnName="id")
     * })
     */
    private $mainRole;

    /**
     * @var \NrBonusJob
     *
     *
     * @ORM\ManyToOne(targetEntity="NrBonusJob")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="second_bonus_job_id", referencedColumnName="id")
     * })
     */
    private $secondBonusJob;

    /**
     * @var integer
     *
     * @Assert\GreaterThanOrEqual(
     *     message="Vous ne pouvez pas descendre en-dessous de 1",
     *     value=1)
     * @Assert\LessThanOrEqual(
     *     message="Vous ne pouvez pas monter au-dessus de 300",
     *     value=300)
     *
     * @ORM\Column(name="second_bonus_job_level", type="smallint", nullable=true)
     */
    private $secondBonusJobLevel;

    /**
     * @var \NrMainJob
     *
     * @ORM\ManyToOne(targetEntity="NrMainJob")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="second_main_job_id", referencedColumnName="id")
     * })
     */
    private $secondMainJob;

    /**
     * @var integer
     *
     * @Assert\GreaterThanOrEqual(
     *     message="Vous ne pouvez pas descendre en-dessous de 1",
     *     value=1)
     * @Assert\LessThanOrEqual(
     *     message="Vous ne pouvez pas monter au-dessus de 300",
     *     value=300)
     *
     * @ORM\Column(name="second_main_job_level", type="smallint", nullable=true)
     */
    private $secondMainJobLevel;

    /**
     * @var \NrBonusJob
     *
     * @ORM\ManyToOne(targetEntity="NrBonusJob")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="third_bonus_job_id", referencedColumnName="id")
     * })
     */
    private $thirdBonusJob;

    /**
     * @var integer
     *
     * @Assert\GreaterThanOrEqual(
     *     message="Vous ne pouvez pas descendre en-dessous de 1",
     *     value=1)
     * @Assert\LessThanOrEqual(
     *     message="Vous ne pouvez pas monter au-dessus de 300",
     *     value=300)
     *
     * @ORM\Column(name="third_bonus_job_level", type="smallint", nullable=true)
     */
    private $thirdBonusJobLevel;

    /**
     * @var \NrUser
     *
     * @ORM\ManyToOne(targetEntity="NrUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * Usercharacter constructor.
     */
    public function __construct()
    {
        $this->dateUpdate = new \DateTime("now");
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
    }

    /**
     * @return bool
     */
    public function isSexe()
    {
        return $this->sexe;
    }

    /**
     * @param bool $sexe
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;
    }

    /**
     * @return bool
     */
    public function isLevel()
    {
        return $this->level;
    }

    /**
     * @param bool $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return bool
     */
    public function isMain()
    {
        return $this->main;
    }

    /**
     * @param bool $main
     */
    public function setMain($main)
    {
        $this->main = $main;
    }

    /**
     * @return string
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * @param string $background
     */
    public function setBackground($background)
    {
        $this->background = $background;
    }

    /**
     * @return string
     */
    public function getOfficialLink()
    {
        return $this->officialLink;
    }

    /**
     * @param string $officialLink
     */
    public function setOfficialLink($officialLink)
    {
        $this->officialLink = $officialLink;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * @return $this
     */
    public function setDateUpdate()
    {
        $this->dateUpdate = new \DateTime("now");
        return $this;
    }

    /**
     * @return int
     */
    public function getDkpTotal()
    {
        return $this->dkpTotal;
    }

    /**
     * @param int $dkpTotal
     */
    public function setDkpTotal($dkpTotal)
    {
        $this->dkpTotal = $dkpTotal;
    }

    /**
     * @return \NrClass
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param \NrClass $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * @return \NrBonusJob
     */
    public function getFirstBonusJob()
    {
        return $this->firstBonusJob;
    }

    /**
     * @param \NrBonusJob $firstBonusJob
     */
    public function setFirstBonusJob($firstBonusJob)
    {
        $this->firstBonusJob = $firstBonusJob;
    }

    /**
     * @return \NrMainJob
     */
    public function getFirstMainJob()
    {
        return $this->firstMainJob;
    }

    /**
     * @param \NrMainJob $firstMainJob
     */
    public function setFirstMainJob($firstMainJob)
    {
        $this->firstMainJob = $firstMainJob;
    }

    /**
     * @return \NrRace
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * @param \NrRace $race
     */
    public function setRace($race)
    {
        $this->race = $race;
    }

    /**
     * @return \NrRole
     */
    public function getMainRole()
    {
        return $this->mainRole;
    }

    /**
     * @param \NrRole $mainRole
     */
    public function setMainRole($mainRole)
    {
        $this->mainRole = $mainRole;
    }

    /**
     * @return \NrBonusJob
     */
    public function getSecondBonusJob()
    {
        return $this->secondBonusJob;
    }

    /**
     * @param \NrBonusJob $secondBonusJob
     */
    public function setSecondBonusJob($secondBonusJob)
    {
        $this->secondBonusJob = $secondBonusJob;
    }

    /**
     * @return \NrMainJob
     */
    public function getSecondMainJob()
    {
        return $this->secondMainJob;
    }

    /**
     * @param \NrMainJob $secondMainJob
     */
    public function setSecondMainJob($secondMainJob)
    {
        $this->secondMainJob = $secondMainJob;
    }

    /**
     * @return \NrBonusJob
     */
    public function getThirdBonusJob()
    {
        return $this->thirdBonusJob;
    }

    /**
     * @param \NrBonusJob $thirdBonusJob
     */
    public function setThirdBonusJob($thirdBonusJob)
    {
        $this->thirdBonusJob = $thirdBonusJob;
    }

    /**
     * @return \NrUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \NrUser $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getFirstBonusJobLevel()
    {
        return $this->firstBonusJobLevel;
    }/**
 * @param int $firstBonusJobLevel
 */
    public function setFirstBonusJobLevel($firstBonusJobLevel)
    {
        $this->firstBonusJobLevel = $firstBonusJobLevel;
    }/**
 * @return int
 */
    public function getFirstMainJobLevel()
    {
        return $this->firstMainJobLevel;
    }/**
 * @param int $firstMainJobLevel
 */
    public function setFirstMainJobLevel($firstMainJobLevel)
    {
        $this->firstMainJobLevel = $firstMainJobLevel;
    }/**
 * @return int
 */
    public function getSecondBonusJobLevel()
    {
        return $this->secondBonusJobLevel;
    }/**
 * @param int $secondBonusJobLevel
 */
    public function setSecondBonusJobLevel($secondBonusJobLevel)
    {
        $this->secondBonusJobLevel = $secondBonusJobLevel;
    }/**
 * @return int
 */
    public function getSecondMainJobLevel()
    {
        return $this->secondMainJobLevel;
    }/**
 * @param int $secondMainJobLevel
 */
    public function setSecondMainJobLevel($secondMainJobLevel)
    {
        $this->secondMainJobLevel = $secondMainJobLevel;
    }/**
 * @return int
 */
    public function getThirdBonusJobLevel()
    {
        return $this->thirdBonusJobLevel;
    }/**
 * @param int $thirdBonusJobLevel
 */
    public function setThirdBonusJobLevel($thirdBonusJobLevel)
    {
        $this->thirdBonusJobLevel = $thirdBonusJobLevel;
    }


    /**
     * @return mixed
     *
     */
    public function __toString() {
        return $this->nickname;
    }
}

