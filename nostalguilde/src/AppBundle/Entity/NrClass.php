<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NrClass
 *
 * @ORM\Table(name="nr_class", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"}), @ORM\UniqueConstraint(name="label_UNIQUE", columns={"label"})})
 * @ORM\Entity
 */
class NrClass
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=45, nullable=false)
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="wow_version", type="boolean", nullable=false)
     */
    private $wowVersion = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="logo_path", type="string", length=255, nullable=true)
     */
    private $logoPath;

    /**
     * @var boolean
     *
     * @ORM\Column(name="side", type="boolean", nullable=true)
     */
    private $side;

//
//    /**
//        * @var \Doctrine\Common\Collections\Collection
//        *
//        * @ORM\ManyToMany(targetEntity="NrGuild", mappedBy="class")
//     */
//    private $guild;
//    /**
//     * Constructor
//     */
//    public function __construct()
//    {
//        $this->guild = new \Doctrine\Common\Collections\ArrayCollection();
//    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return bool
     */
    public function isWowVersion()
    {
        return $this->wowVersion;
    }

    /**
     * @return string
     */
    public function getLogoPath()
    {
        return $this->logoPath;
    }

    /**
     * @return bool
     */
    public function isSide()
    {
        return $this->side;
    }

    /**
     * @param bool $side
     */
    public function setSide($side)
    {
        $this->side = $side;
    }

//    /**
//     * @return \Doctrine\Common\Collections\Collection
//     */
//    public function getGuild()
//    {
//        return $this->guild;
//    }

    /**
     * @return mixed
     *
     */
    public function __toString() {
        return $this->label;
    }

}

