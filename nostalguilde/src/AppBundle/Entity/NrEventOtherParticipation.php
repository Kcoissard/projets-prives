<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NrEventOtherParticipation
 *
 * @ORM\Table(name="nr_event_other_participation", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_event_other_participation-character_id_idx", columns={"character_id"}), @ORM\Index(name="fk_event_other_participation-chosen_role_id_idx", columns={"chosen_role_id"}), @ORM\Index(name="fk_event_other_participation-user_id_idx", columns={"user_id"}), @ORM\Index(name="fk_event_other_participation-event_pvm_id_idx", columns={"event_other_id"})})
 * @ORM\Entity
 */
class NrEventOtherParticipation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="subscription_date", type="datetime", nullable=true)
     */
    private $subscriptionDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="not_sure", type="boolean", nullable=true)
     */
    private $notSure = '0';

    /**
     * @var \NrCharacter
     *
     * @ORM\ManyToOne(targetEntity="NrCharacter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="character_id", referencedColumnName="id")
     * })
     */
    private $character;

    /**
     * @var \NrRole
     *
     * @ORM\ManyToOne(targetEntity="NrRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="chosen_role_id", referencedColumnName="id")
     * })
     */
    private $chosenRole;

    /**
     * @var \NrGuildOtherEvent
     *
     * @ORM\ManyToOne(targetEntity="NrGuildOtherEvent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_other_id", referencedColumnName="id")
     * })
     */
    private $eventOther;

    /**
     * @var \NrUser
     *
     * @ORM\ManyToOne(targetEntity="NrUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}

