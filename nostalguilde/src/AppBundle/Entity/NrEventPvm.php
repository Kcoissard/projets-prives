<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NrEventPvm
 *
 * @ORM\Table(name="nr_event_pvm", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"}), @ORM\UniqueConstraint(name="label_UNIQUE", columns={"label"})})
 * @ORM\Entity
 */
class NrEventPvm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=80, nullable=false)
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="medium_player_number", type="boolean", nullable=false)
     */
    private $mediumPlayerNumber = '5';

    /**
     * @var string
     *
     * @ORM\Column(name="type_pvm", type="string", length=25, nullable=false)
     */
    private $typePvm;

    /**
     * @var boolean
     *
     * @ORM\Column(name="wow_version", type="boolean", nullable=false)
     */
    private $wowVersion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="minimum_level", type="boolean", nullable=true)
     */
    private $minimumLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="game_zone", type="string", length=100, nullable=true)
     */
    private $gameZone;

    /**
     * @var string
     *
     * @ORM\Column(name="link_detail_fr", type="string", length=255, nullable=true)
     */
    private $linkDetailFr;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="image_path", type="string", length=255, nullable=true)
     */
    private $imagePath;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_path", type="string", length=255, nullable=true)
     */
    private $logoPath;

    /**
     * @var integer
     *
     * @ORM\Column(name="dkp_point", type="integer", nullable=true)
     */
    private $dkpPoint = '0';


}

