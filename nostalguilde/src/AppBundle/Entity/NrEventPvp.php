<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NrEventPvp
 *
 * @ORM\Table(name="nr_event_pvp", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"}), @ORM\UniqueConstraint(name="label_UNIQUE", columns={"label"})})
 * @ORM\Entity
 */
class NrEventPvp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=80, nullable=false)
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="medium_player_number", type="boolean", nullable=true)
     */
    private $mediumPlayerNumber = '5';

    /**
     * @var string
     *
     * @ORM\Column(name="type_pvp", type="string", length=25, nullable=false)
     */
    private $typePvp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="wow_version", type="boolean", nullable=false)
     */
    private $wowVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="image_path", type="string", length=255, nullable=true)
     */
    private $imagePath;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_path", type="string", length=255, nullable=true)
     */
    private $logoPath;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tournament", type="boolean", nullable=true)
     */
    private $tournament = '0';


}

