<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NrEventPvpComment
 *
 * @ORM\Table(name="nr_event_pvp_comment", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_event_pvp_comment-event_pvp_id_idx", columns={"event_pvp_id"}), @ORM\Index(name="fk_event_pvp_comment-user_id_idx", columns={"user_id"})})
 * @ORM\Entity
 */
class NrEventPvpComment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=false)
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=true)
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     */
    private $updateDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="important_message", type="boolean", nullable=true)
     */
    private $importantMessage = '0';

    /**
     * @var \NrGuildPvpEvent
     *
     * @ORM\ManyToOne(targetEntity="NrGuildPvpEvent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_pvp_id", referencedColumnName="id")
     * })
     */
    private $eventPvp;

    /**
     * @var \NrUser
     *
     * @ORM\ManyToOne(targetEntity="NrUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}

