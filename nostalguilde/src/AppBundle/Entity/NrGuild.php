<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NrGuild
 *
 * @ORM\Table(name="nr_guild", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_guild-server_id_idx", columns={"server_id"})})
 * @ORM\Entity
 */
class NrGuild
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    private $isActive = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=24, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="side", type="boolean", nullable=true)
     */
    private $side = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="recruitment_active", type="boolean", nullable=true)
     */
    private $recruitmentActive = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="registration_pwd", type="string", length=20, nullable=false)
     */
    private $registrationPwd;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_path", type="string", length=255, nullable=true)
     */
    private $logoPath;

    /**
     * @var string
     *
     * @ORM\Column(name="connexion_image_path", type="string", length=255, nullable=true)
     */
    private $connexionImagePath;

    /**
     * @var string
     *
     * @ORM\Column(name="slogan", type="string", length=255, nullable=true)
     */
    private $slogan;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="info_guild", type="text", nullable=true)
     */
    private $infoGuild;

    /**
     * @var string
     *
     * @ORM\Column(name="main_color", type="string", length=6, nullable=true)
     */
    private $mainColor = '0e02ad';

    /**
     * @var string
     *
     * @ORM\Column(name="second_color", type="string", length=6, nullable=true)
     */
    private $secondColor = 'eae502';

    /**
     * @var string
     *
     * @ORM\Column(name="mail_gm", type="string", length=100, nullable=false)
     */
    private $mailGm;

    /**
     * @var string
     *
     * @ORM\Column(name="groupcalendar_canal", type="string", length=7, nullable=true)
     */
    private $groupcalendarCanal;

    /**
     * @var string
     *
     * @ORM\Column(name="groupcalendar_pwd", type="string", length=6, nullable=true)
     */
    private $groupcalendarPwd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dkp_system_active", type="boolean", nullable=true)
     */
    private $dkpSystemActive = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="forum_rules", type="text", nullable=true)
     */
    private $forumRules;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube_chain", type="string", length=255, nullable=true)
     */
    private $youtubeChain;

    /**
     * @var string
     *
     * @ORM\Column(name="stream_link", type="string", length=255, nullable=true)
     */
    private $streamLink;

    /**
     * @var \NrServer
     *
     * @ORM\ManyToOne(targetEntity="NrServer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="server_id", referencedColumnName="id")
     * })
     */
    private $server;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="NrGuild", inversedBy="guild1")
     * @ORM\JoinTable(name="nr_guild_alliance",
     *   joinColumns={
     *     @ORM\JoinColumn(name="guild_id_1", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="guild_id_2", referencedColumnName="id")
     *   }
     * )
     */
    private $guild2;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="NrClass", inversedBy="guild")
     * @ORM\JoinTable(name="nr_guild_class_recruitment",
     *   joinColumns={
     *     @ORM\JoinColumn(name="guild_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="class_id", referencedColumnName="id")
     *   }
     * )
     */
    private $class;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->guild2 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->class = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isSide()
    {
        return $this->side;
    }

    /**
     * @param bool $side
     */
    public function setSide($side)
    {
        $this->side = $side;
    }

    /**
     * @return bool
     */
    public function isRecruitmentActive()
    {
        return $this->recruitmentActive;
    }

    /**
     * @param bool $recruitmentActive
     */
    public function setRecruitmentActive($recruitmentActive)
    {
        $this->recruitmentActive = $recruitmentActive;
    }

    /**
     * @return string
     */
    public function getRegistrationPwd()
    {
        return $this->registrationPwd;
    }

    /**
     * @param string $registrationPwd
     */
    public function setRegistrationPwd($registrationPwd)
    {
        $this->registrationPwd = $registrationPwd;
    }

    /**
     * @return string
     */
    public function getLogoPath()
    {
        return $this->logoPath;
    }

    /**
     * @param string $logoPath
     */
    public function setLogoPath($logoPath)
    {
        $this->logoPath = $logoPath;
    }

    /**
     * @return string
     */
    public function getConnexionImagePath()
    {
        return $this->connexionImagePath;
    }

    /**
     * @param string $connexionImagePath
     */
    public function setConnexionImagePath($connexionImagePath)
    {
        $this->connexionImagePath = $connexionImagePath;
    }

    /**
     * @return string
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    /**
     * @param string $slogan
     */
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getInfoGuild()
    {
        return $this->infoGuild;
    }

    /**
     * @param string $infoGuild
     */
    public function setInfoGuild($infoGuild)
    {
        $this->infoGuild = $infoGuild;
    }

    /**
     * @return string
     */
    public function getMainColor()
    {
        return $this->mainColor;
    }

    /**
     * @param string $mainColor
     */
    public function setMainColor($mainColor)
    {
        $this->mainColor = $mainColor;
    }

    /**
     * @return string
     */
    public function getSecondColor()
    {
        return $this->secondColor;
    }

    /**
     * @param string $secondColor
     */
    public function setSecondColor($secondColor)
    {
        $this->secondColor = $secondColor;
    }

    /**
     * @return string
     */
    public function getMailGm()
    {
        return $this->mailGm;
    }

    /**
     * @param string $mailGm
     */
    public function setMailGm($mailGm)
    {
        $this->mailGm = $mailGm;
    }

    /**
     * @return string
     */
    public function getGroupcalendarCanal()
    {
        return $this->groupcalendarCanal;
    }

    /**
     * @param string $groupcalendarCanal
     */
    public function setGroupcalendarCanal($groupcalendarCanal)
    {
        $this->groupcalendarCanal = $groupcalendarCanal;
    }

    /**
     * @return string
     */
    public function getGroupcalendarPwd()
    {
        return $this->groupcalendarPwd;
    }

    /**
     * @param string $groupcalendarPwd
     */
    public function setGroupcalendarPwd($groupcalendarPwd)
    {
        $this->groupcalendarPwd = $groupcalendarPwd;
    }

    /**
     * @return bool
     */
    public function isDkpSystemActive()
    {
        return $this->dkpSystemActive;
    }

    /**
     * @param bool $dkpSystemActive
     */
    public function setDkpSystemActive($dkpSystemActive)
    {
        $this->dkpSystemActive = $dkpSystemActive;
    }

    /**
     * @return string
     */
    public function getForumRules()
    {
        return $this->forumRules;
    }

    /**
     * @param string $forumRules
     */
    public function setForumRules($forumRules)
    {
        $this->forumRules = $forumRules;
    }

    /**
     * @return string
     */
    public function getYoutubeChain()
    {
        return $this->youtubeChain;
    }

    /**
     * @param string $youtubeChain
     */
    public function setYoutubeChain($youtubeChain)
    {
        $this->youtubeChain = $youtubeChain;
    }

    /**
     * @return string
     */
    public function getStreamLink()
    {
        return $this->streamLink;
    }

    /**
     * @param string $streamLink
     */
    public function setStreamLink($streamLink)
    {
        $this->streamLink = $streamLink;
    }

    /**
     * @return \NrServer
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @param \NrServer $server
     */
    public function setServer($server)
    {
        $this->server = $server;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGuild2()
    {
        return $this->guild2;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $guild2
     */
    public function setGuild2($guild2)
    {
        $this->guild2 = $guild2;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }



}

