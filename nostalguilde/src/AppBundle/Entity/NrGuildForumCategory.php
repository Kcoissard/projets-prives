<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NrGuildForumCategory
 *
 * @ORM\Table(name="nr_guild_forum_category", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_guild_forum_category-guild_id_idx", columns={"guild_id"}), @ORM\Index(name="fk_guild_forum_category-author_id_idx", columns={"authord_id"})})
 * @ORM\Entity
 */
class NrGuildForumCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=20, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="string", length=50, nullable=true)
     */
    private $shortDescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publication_date", type="datetime", nullable=true)
     */
    private $publicationDate;

    /**
     * @var \NrUser
     *
     * @ORM\ManyToOne(targetEntity="NrUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="authord_id", referencedColumnName="id")
     * })
     */
    private $authord;

    /**
     * @var \NrGuild
     *
     * @ORM\ManyToOne(targetEntity="NrGuild")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="guild_id", referencedColumnName="id")
     * })
     */
    private $guild;


}

