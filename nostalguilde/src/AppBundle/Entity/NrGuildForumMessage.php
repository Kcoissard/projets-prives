<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NrGuildForumMessage
 *
 * @ORM\Table(name="nr_guild_forum_message", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_guild_forum_message-topic_id_idx", columns={"topic_id"}), @ORM\Index(name="fk_guild_forum_message-author_id_idx", columns={"author_id"}), @ORM\Index(name="fk_guild_forum_message-message_response_id_idx", columns={"message_response_id"})})
 * @ORM\Entity
 */
class NrGuildForumMessage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=false)
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publication_date", type="datetime", nullable=false)
     */
    private $publicationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     */
    private $updateDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="first_message", type="boolean", nullable=true)
     */
    private $firstMessage = '0';

    /**
     * @var \NrUser
     *
     * @ORM\ManyToOne(targetEntity="NrUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     * })
     */
    private $author;

    /**
     * @var \NrGuildForumMessage
     *
     * @ORM\ManyToOne(targetEntity="NrGuildForumMessage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="message_response_id", referencedColumnName="id")
     * })
     */
    private $messageResponse;

    /**
     * @var \NrGuildForumTopic
     *
     * @ORM\ManyToOne(targetEntity="NrGuildForumTopic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="topic_id", referencedColumnName="id")
     * })
     */
    private $topic;


}

