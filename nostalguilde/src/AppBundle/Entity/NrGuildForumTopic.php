<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NrGuildForumTopic
 *
 * @ORM\Table(name="nr_guild_forum_topic", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_guild_forum_topic-category_id_idx", columns={"category_id"}), @ORM\Index(name="fk_guild_forum_topic-author_id_idx", columns={"author_id"})})
 * @ORM\Entity
 */
class NrGuildForumTopic
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50, nullable=false)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publication_date", type="datetime", nullable=false)
     */
    private $publicationDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_response_id", type="integer", nullable=true)
     */
    private $lastResponseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="view_counter", type="integer", nullable=true)
     */
    private $viewCounter = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="pinned", type="boolean", nullable=true)
     */
    private $pinned = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="authorized_response", type="boolean", nullable=true)
     */
    private $authorizedResponse = '1';

    /**
     * @var \NrUser
     *
     * @ORM\ManyToOne(targetEntity="NrUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     * })
     */
    private $author;

    /**
     * @var \NrGuildForumCategory
     *
     * @ORM\ManyToOne(targetEntity="NrGuildForumCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;


}

