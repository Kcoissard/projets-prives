<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NrGuildPvmEvent
 *
 * @ORM\Table(name="nr_guild_pvm_event", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_guild_pvm_event-event_pvm_id_idx", columns={"event_pvm_id"}), @ORM\Index(name="fk_guild_pvm_event-guild_id_idx", columns={"guild_id"}), @ORM\Index(name="fk_guild_pvm_event-author_id_idx", columns={"author_id"}), @ORM\Index(name="fk_guild_pvm_event-responsable_id_idx", columns={"responsable_id"})})
 * @ORM\Entity
 */
class NrGuildPvmEvent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=false)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="min_level", type="boolean", nullable=true)
     */
    private $minLevel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="max_level", type="boolean", nullable=true)
     */
    private $maxLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="information_text", type="text", nullable=true)
     */
    private $informationText;

    /**
     * @var string
     *
     * @ORM\Column(name="strategy_link", type="string", length=255, nullable=true)
     */
    private $strategyLink;

    /**
     * @var boolean
     *
     * @ORM\Column(name="open_to_guild_alliance", type="boolean", nullable=true)
     */
    private $openToGuildAlliance = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="open_to_everyone", type="boolean", nullable=true)
     */
    private $openToEveryone = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="dkp_given", type="boolean", nullable=true)
     */
    private $dkpGiven = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="places_number_limit", type="smallint", nullable=true)
     */
    private $placesNumberLimit;

    /**
     * @var \NrUser
     *
     * @ORM\ManyToOne(targetEntity="NrUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     * })
     */
    private $author;

    /**
     * @var \NrEventPvm
     *
     * @ORM\ManyToOne(targetEntity="NrEventPvm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_pvm_id", referencedColumnName="id")
     * })
     */
    private $eventPvm;

    /**
     * @var \NrGuild
     *
     * @ORM\ManyToOne(targetEntity="NrGuild")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="guild_id", referencedColumnName="id")
     * })
     */
    private $guild;

    /**
     * @var \NrUser
     *
     * @ORM\ManyToOne(targetEntity="NrUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="responsable_id", referencedColumnName="id")
     * })
     */
    private $responsable;


}

