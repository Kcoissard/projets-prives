<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NrMainJob
 *
 * @ORM\Table(name="nr_main_job", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})})
 * @ORM\Entity
 */
class NrMainJob
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=50, nullable=false)
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="wow_version", type="boolean", nullable=false)
     */
    private $wowVersion = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="max_level", type="smallint", nullable=false)
     */
    private $maxLevel = '300';

    /**
     * @return bool
     */
    public function isId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return bool
     */
    public function isWowVersion()
    {
        return $this->wowVersion;
    }

    /**
     * @return int
     */
    public function getMaxLevel()
    {
        return $this->maxLevel;
    }

    public function __toString() {
        return $this->label;
    }

}

