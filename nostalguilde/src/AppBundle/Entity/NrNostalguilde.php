<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NrNostalguilde
 *
 * @ORM\Table(name="nr_nostalguilde", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})})
 * @ORM\Entity
 */
class NrNostalguilde
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    private $isActive = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_maintenance", type="boolean", nullable=true)
     */
    private $isMaintenance = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="wow_version", type="boolean", nullable=false)
     */
    private $wowVersion = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="classic_active", type="boolean", nullable=true)
     */
    private $classicActive = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="admin_mail", type="string", length=80, nullable=true)
     */
    private $adminMail = 'nostalguilde.noobs.revenge@gmail.com';

    /**
     * @var boolean
     *
     * @ORM\Column(name="dkp_system_active", type="boolean", nullable=true)
     */
    private $dkpSystemActive = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="groupcalendar_active", type="boolean", nullable=true)
     */
    private $groupcalendarActive = '0';


}

