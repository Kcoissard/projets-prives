<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NrServer
 *
 * @ORM\Table(name="nr_server", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})})
 * @ORM\Entity
 */
class NrServer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=100, nullable=false)
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="official", type="boolean", nullable=true)
     */
    private $official = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="wow_version", type="boolean", nullable=true)
     */
    private $wowVersion = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="PVP", type="boolean", nullable=true)
     */
    private $pvp = '1';


}

