<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * NrUser
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NrUserRepository")
 *
 * @ORM\Table(name="nr_user", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_guild_user-guild_id_idx", columns={"guild_id"})})
 */
class NrUser extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="guild_validation", type="boolean", nullable=true)
     */
    private $guildValidation = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="message_inscription", type="string", length=255, nullable=true)
     */
    private $messageInscription;

    /**
     * @var boolean
     *
     * @ORM\Column(name="agree_gravatar", type="boolean", nullable=true)
     */
    private $agreeGravatar = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="image_path", type="string", length=255, nullable=true)
     */
    private $imagePath;

    /**
     * @var string
     *
     * @ORM\Column(name="personnal_description", type="text", length=16777215, nullable=true)
     */
    private $personnalDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="approximative_dispo", type="string", length=255, nullable=true)
     */
    private $approximativeDispo;

    /**
     * @var string
     *
     * @ORM\Column(name="favourite_music", type="text", nullable=true)
     */
    private $favouriteMusic;

    /**
     * @var string
     *
     * @ORM\Column(name="favourite_movie", type="text", nullable=true)
     */
    private $favouriteMovie;

    /**
     * @var string
     *
     * @ORM\Column(name="favourite_book", type="text", nullable=true)
     */
    private $favouriteBook;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=true)
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="event_counter", type="integer", nullable=true)
     */
    private $eventCounter = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="forum_topic_counter", type="integer", nullable=true)
     */
    private $forumTopicCounter = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="forum_message_counter", type="integer", nullable=true)
     */
    private $forumMessageCounter = '0';

    /**
     * @var \NrGuild
     *
     * @ORM\ManyToOne(targetEntity="NrGuild")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="guild_id", referencedColumnName="id")
     * })
     */
    private $guild;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="NrNewWebsiteFeature", mappedBy="idUser")
     */
    private $idNewFeature;


    public function __construct()
    {
        parent::__construct();
        $this->idNewFeature = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return bool
     */
    public function isGuildValidation()
    {
        return $this->guildValidation;
    }

    /**
     * @param bool $guildValidation
     */
    public function setGuildValidation($guildValidation)
    {
        $this->guildValidation = $guildValidation;
    }

    /**
     * @return string
     */
    public function getMessageInscription()
    {
        return $this->messageInscription;
    }

    /**
     * @param string $messageInscription
     */
    public function setMessageInscription($messageInscription)
    {
        $this->messageInscription = $messageInscription;
    }

    /**
     * @return bool
     */
    public function isAgreeGravatar()
    {
        return $this->agreeGravatar;
    }

    /**
     * @param bool $agreeGravatar
     */
    public function setAgreeGravatar($agreeGravatar)
    {
        $this->agreeGravatar = $agreeGravatar;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * @param string $imagePath
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;
    }

    /**
     * @return string
     */
    public function getPersonnalDescription()
    {
        return $this->personnalDescription;
    }

    /**
     * @param string $personnalDescription
     */
    public function setPersonnalDescription($personnalDescription)
    {
        $this->personnalDescription = $personnalDescription;
    }

    /**
     * @return string
     */
    public function getApproximativeDispo()
    {
        return $this->approximativeDispo;
    }

    /**
     * @param string $approximativeDispo
     */
    public function setApproximativeDispo($approximativeDispo)
    {
        $this->approximativeDispo = $approximativeDispo;
    }

    /**
     * @return string
     */
    public function getFavouriteMusic()
    {
        return $this->favouriteMusic;
    }

    /**
     * @param string $favouriteMusic
     */
    public function setFavouriteMusic($favouriteMusic)
    {
        $this->favouriteMusic = $favouriteMusic;
    }

    /**
     * @return string
     */
    public function getFavouriteMovie()
    {
        return $this->favouriteMovie;
    }

    /**
     * @param string $favouriteMovie
     */
    public function setFavouriteMovie($favouriteMovie)
    {
        $this->favouriteMovie = $favouriteMovie;
    }

    /**
     * @return string
     */
    public function getFavouriteBook()
    {
        return $this->favouriteBook;
    }

    /**
     * @param string $favouriteBook
     */
    public function setFavouriteBook($favouriteBook)
    {
        $this->favouriteBook = $favouriteBook;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param \DateTime $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * @param \DateTime $dateUpdate
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;
    }

    /**
     * @return int
     */
    public function getEventCounter()
    {
        return $this->eventCounter;
    }

    /**
     * @param int $eventCounter
     */
    public function setEventCounter($eventCounter)
    {
        $this->eventCounter = $eventCounter;
    }

    /**
     * @return int
     */
    public function getForumTopicCounter()
    {
        return $this->forumTopicCounter;
    }

    /**
     * @param int $forumTopicCounter
     */
    public function setForumTopicCounter($forumTopicCounter)
    {
        $this->forumTopicCounter = $forumTopicCounter;
    }

    /**
     * @return int
     */
    public function getForumMessageCounter()
    {
        return $this->forumMessageCounter;
    }

    /**
     * @param int $forumMessageCounter
     */
    public function setForumMessageCounter($forumMessageCounter)
    {
        $this->forumMessageCounter = $forumMessageCounter;
    }

    /**
     * @return \NrGuild
     */
    public function getGuild()
    {
        return $this->guild;
    }

    /**
     * @param \NrGuild $guild
     */
    public function setGuild($guild)
    {
        $this->guild = $guild;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdNewFeature()
    {
        return $this->idNewFeature;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $idNewFeature
     */
    public function setIdNewFeature($idNewFeature)
    {
        $this->idNewFeature = $idNewFeature;
    }


}

