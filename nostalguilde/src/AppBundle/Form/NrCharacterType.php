<?php

namespace AppBundle\Form;

use AppBundle\Entity\NrCharacter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Doctrine\Common\Persistence\ObjectManager;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;


use Doctrine\ORM\EntityRepository;
use AppBundle\Repository\GlobalConfigRepository;

use Symfony\Component\Validator\Constraints\Length;

class NrCharacterType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * JustAFormType constructor.
     *
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //$wowversion = intval($this->container->getParameter('AppBundle.wow_version'));
        $globalConfigRepository = new GlobalConfigRepository();
        $wowversion = $globalConfigRepository->getWowVersion();
        $allianceOnly = $globalConfigRepository->isAllianceOnly();

        $builder
            ->add('nickname', TextType::class, array(
                'label' => 'Pseudo du personnage',
                'required' => true,
            ))
            ->add('sexe', ChoiceType::class, array(
                'label' => 'Sexe apparent ?',
                'choices' => array('Oui' => true, 'Non' => false),
                'expanded' => true,
                'multiple' => false,
                'required' => true,
            ))
            ->add('level', IntegerType::class, array(
                'label' => "Niveau du personnage",
                'attr' => [
                    'min' => 1,
                    'max' => 60,
                    'step' => 1,
                ],
                'required' => true,
            ))
            ->add('mainRole',  EntityType::class, array(
                'label' => 'Rôle favori',
                'class' => 'AppBundle:NrRole',
                'choice_label' => 'label',
                'multiple' => false,
                'expanded' => true,
                'required' => true,
            ))
            ->add('firstMainJob',  EntityType::class, array(
                'label' => 'Premier métier principal',
                'class' => 'AppBundle:NrMainJob',
                'choice_label' => 'label',
                'placeholder' => ' ',
                'query_builder' => function(EntityRepository $er) use ($wowversion) {
                    return $er->createQueryBuilder("mj")
                        ->where("mj.wowVersion = :wowversion")
                        ->setParameter('wowversion', $wowversion);
                },
                'multiple'  => false,
                'expanded' => false,
            ))
            ->add('firstMainJobLevel', IntegerType::class, array(
                'label' => "Niveau du métier",
                'attr' => [
                    'min' => 0,
                    'max' => 300,
                    'step' =>1,
                ],
            ))
            ->add('secondMainJob',  EntityType::class, array(
                'label' => 'Second métier principal',
                'class' => 'AppBundle:NrMainJob',
                'choice_label' => 'label',
                'placeholder' => ' ',
                'query_builder' => function(EntityRepository $er) use ($wowversion) {
                    return $er->createQueryBuilder("mj")
                        ->where("mj.wowVersion = :wowversion")
                        ->setParameter('wowversion', $wowversion);
                },
                'multiple'  => false,
                'expanded' => false,
            ))
            ->add('secondMainJobLevel', IntegerType::class, array(
                'label' => "Niveau du métier",
                'attr' => [
                    'min' => 0,
                    'max' => 300,
                    'step' =>1,
                ],
            ))
            ->add('firstBonusJob',  EntityType::class, array(
                'label' => 'Premier métier secondaire',
                'class' => 'AppBundle:NrBonusJob',
                'choice_label' => 'label',
                'placeholder' => ' ',
                'query_builder' => function(EntityRepository $er) use ($wowversion) {
                    return $er->createQueryBuilder("mj")
                        ->where("mj.wowVersion = :wowversion")
                        ->setParameter('wowversion', $wowversion);
                },
                'multiple'  => false,
                'expanded' => false,
            ))
            ->add('firstBonusJobLevel', IntegerType::class, array(
                'label' => "Niveau du métier",
                'attr' => [
                    'min' => 0,
                    'max' => 300,
                    'step' =>1,
                ],
            ))
            ->add('secondBonusJob',  EntityType::class, array(
                'label' => 'Second métier secondaire',
                'class' => 'AppBundle:NrBonusJob',
                'choice_label' => 'label',
                'placeholder' => ' ',
                'query_builder' => function(EntityRepository $er) use ($wowversion) {
                    return $er->createQueryBuilder("mj")
                        ->where("mj.wowVersion = :wowversion")
                        ->setParameter('wowversion', $wowversion);
                },
                'multiple'  => false,
                'expanded' => false,
                'required' => false,
            ))
            ->add('secondBonusJobLevel', IntegerType::class, array(
                'label' => "Niveau du métier",
                'attr' => [
                    'min' => 0,
                    'max' => 300,
                    'step' =>1,
                ],
            ))
            ->add('thirdBonusJob',  EntityType::class, array(
                'label' => 'Troisième métier secondaire',
                'class' => 'AppBundle:NrBonusJob',
                'choice_label' => 'label',
                'placeholder' => ' ',
                'query_builder' => function(EntityRepository $er) use ($wowversion) {
                    return $er->createQueryBuilder("mj")
                        ->where("mj.wowVersion = :wowversion")
                        ->setParameter('wowversion', $wowversion);
                },
                'multiple'  => false,
                'expanded' => false,
            ))
            ->add('thirdBonusJobLevel', IntegerType::class, array(
                'label' => "Niveau du métier",
                'attr' => [
                    'min' => 0,
                    'max' => 300,
                    'step' =>1,
                ],
            ))
            ->add('background', CKEditorType::class, array(
                //'attr' => array('class' => 'tinymce'),
                'label' => '(RP) Histoire du personnage',
                'required' => false,
            ))
            ->add('officialLink', TextType::class, array(
                'label' => 'Lien vers l\'armurerie du personnage (ex: https://www.nostalgeek-serveur.com/index.php?box=armory&character=556083)',
            ))
        ;

        //if global conf repo -> alliance only
        if ($allianceOnly) {
            $builder->add('race',  EntityType::class, array(
                'label' => 'Race du personnage',
                'class' => 'AppBundle:NrRace',
                'choice_label' => 'label',
                'placeholder' => false,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder("char")
                        ->where("char.side = 0")
                        ->orWhere("char.side is null");
                },
                'multiple' => false,
                'expanded' => true,
                'required' => true,
            ))
            ->add('class',  EntityType::class, array(
                'label' => 'Classe du personnage',
                'class' => 'AppBundle:NrClass',
                'choice_label' => 'label',
                'placeholder' => false,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder("char")
                        ->where("char.side = 0")
                        ->orWhere("char.side is null");
                },
                'multiple' => false,
                'expanded' => true,
                'required' => true,
            ));
        } else {
            $builder->add('race',  EntityType::class, array(
                'label' => 'Race du personnage',
                'class' => 'AppBundle:NrRace',
                'choice_label' => 'label',
                'multiple' => false,
                'expanded' => true,
                'required' => true,
            ))
            ->add('class',  EntityType::class, array(
                'label' => 'Classe du personnage',
                'class' => 'AppBundle:NrClass',
                'choice_label' => 'label',
                'multiple' => false,
                'expanded' => true,
                'required' => true,
            ));
        }

        //if user dont have character => main by default
        $userAlreadyHaveMainCharacter = $options['userAlreadyHaveMainCharacter'];
        $edit = $options['editing'];

        if ($edit) {
            if (!$userAlreadyHaveMainCharacter) {
                $builder->add('main', ChoiceType::class, array(
                    'label'    => 'Personnage principal ?',
                    'choices' => array('Oui' => true, 'Non' => false),
                    'expanded' => true,
                    'multiple' => false,
                    'placeholder' => false,
                    'required' => false,
                ));
            } else {
                $builder->add('main', HiddenType::class, array(
                    'data' => true,
                    'required' => true,
                ));
            }
        } else {
            if ($userAlreadyHaveMainCharacter) {
                $builder->add('main', ChoiceType::class, array(
                    'label'    => 'Personnage principal ?',
                    'choices' => array('Oui' => true, 'Non' => false),
                    'expanded' => true,
                    'multiple' => false,
                    'placeholder' => false,
                    'required' => false,
                ));
            } else {
                $builder->add('main', HiddenType::class, array(
                    'data' => true,
                    'required' => true,
                ));
            }
        }


        //->add('dkpTotal')
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => \AppBundle\Entity\NrCharacter::class,
            'userAlreadyHaveMainCharacter' => null,
            'editing' => null,
//            // enable/disable CSRF protection for this form
//            'csrf_protection' => true,
//            // the name of the hidden HTML field that stores the token
//            'csrf_field_name' => '_token',
//            // an arbitrary string used to generate the value of the token
//            // using a different string for each form improves its security
//            'csrf_token_id'   => 'newCharacter',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_nrcharacter';
    }


}
