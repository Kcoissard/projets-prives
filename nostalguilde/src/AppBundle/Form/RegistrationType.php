<?php
/**
 * Created by PhpStorm.
 * User: FixeNeon
 * Date: 31/05/2018
 * Time: 19:15
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dispo = ['Lundi' => 'MON', 'Mardi' => 'TUE', 'Mercredi' => 'WED', 'Jeudi' => 'THU', 'Vendedi' => 'FRI', 'Samedi' => 'SAT', 'Dimanche' => 'SUN'];

        $builder
        ->add('personnal_description', CKEditorType::class, array(
            //'attr' => array('class' => 'tinymce'),
            'label' => 'Description personnelle',
            'required'  => false,
        ))
        ->add('approximative_dispo', ChoiceType::class, [
            'label' => 'Disponibilités approximatives les soirs de semaine :',
            'choices' => $dispo,
            'multiple' => true,
            'expanded' => true
        ])
        ->add('favourite_music', CKEditorType::class, array(
            //'attr' => array('class' => 'tinymce'),
            'label' => 'Musiques favorites',
            'required'  => false,
        ))
        ->add('favourite_movie', CKEditorType::class, array(
            //'attr' => array('class' => 'tinymce'),
            'label' => 'Films favoris',
            'required'  => false,
        ))
        ->add('favourite_book', CKEditorType::class, array(
            //'attr' => array('class' => 'tinymce'),
            'label' => 'Livres favoris',
            'required'  => false,
        ));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';

    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}