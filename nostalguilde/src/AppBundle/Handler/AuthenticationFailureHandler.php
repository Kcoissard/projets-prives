<?php

namespace AppBundle\Handler;

use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Doctrine\Common\Persistence\ObjectManager;

class AuthenticationFailureHandler implements AuthenticationFailureHandlerInterface {

    protected $router;
    private $em;

    public function __construct(Router $router, ObjectManager $em) {
        $this->router = $router;
        $this->em = $em;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception) {
        // your code here - creating new object. redirects etc.
    }

}