<?php

namespace AppBundle\Handler;

use AppBundle\Entity\NrGuild;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

use AppBundle\Repository\NrUserRepository;

class AuthenticationSuccessHandler implements AuthenticationSuccessHandlerInterface {

    protected $router;
    private $em;

    public function __construct(Router $router, ObjectManager $em) {
        $this->router = $router;
        $this->em = $em;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token) {

        /** @var NrGuild $guild */
        $guild = $token->getUser()->getGuild();
        dump($guild->getId());die();

        //check guild id


        //check guild validation

        //put guild infos in session
        //$session = $this->get('session');
        //$session->set('',

        $response = new RedirectResponse($this->router->generate('character_new'));

        return $response;
    }

}