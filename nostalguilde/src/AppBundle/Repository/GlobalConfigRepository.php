<?php
/**
 * Created by PhpStorm.
 * User: FixeNeon
 * Date: 02/06/2018
 * Time: 10:21
 */

namespace AppBundle\Repository;


class GlobalConfigRepository
{
    public $allianceOnly;
    public $guildId;
    public $wowVersion;

    /**
     * GlobalConfigRepository constructor.
     * @param bool $allianceOnly
     * @param int $guildId
     * @param int $wowVersion
     */
    public function __construct()
    {
        $this->allianceOnly = true;
        $this->guildId = 777;
        $this->wowVersion = 1;
    }

    /**
     * @return bool
     */
    public function isAllianceOnly()
    {
        return $this->allianceOnly;
    }

    /**
     * @return int
     */
    public function getGuildId()
    {
        return $this->guildId;
    }

    /**
     * @return int
     */
    public function getWowVersion()
    {
        return $this->wowVersion;
    }


}