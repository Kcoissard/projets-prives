<?php
/**
 * Created by PhpStorm.
 * User: FixeNeon
 * Date: 01/06/2018
 * Time: 14:26
 */
namespace AppBundle\Repository;

use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Doctrine\ORM\EntityRepository;

class NrGuildRepository extends EntityRepository
{
    /**
     * @param null $guildId
     * @return bool
     */
    public function findById($guildId = null)
    {
        if ($guildId == null) {
            throw new Exception(" - Fonction getGuildById - $.$guildId = null - ");
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT g FROM AppBundle:NrGuild g 
                WHERE g.id=' . $guildId
        );

        $guild = $query->getResult();

        if (!empty($guild)) {
            return $guild[0];
        } else {
            return false;
        }
    }
}