<?php
/**
 * Created by PhpStorm.
 * User: FixeNeon
 * Date: 07/06/2018
 * Time: 13:57
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;


class NrUserRepository extends EntityRepository
{
    /**
     * @param $username
     * @return array|mixed
     */
    public function findUserByUsername($username) {

        $em = $this->getEntityManager();

        return $em->createQuery(
            'SELECT (us) FROM AppBundle:NrUser us 
                WHERE us.username=' . $username
            )
            ->getResult();
    }
}
